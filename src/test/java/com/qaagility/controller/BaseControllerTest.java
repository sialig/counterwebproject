package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;


public class BaseControllerTest {

	@Test
	public void testWelcome() throws Exception {
        	ModelMap map = mock(ModelMap.class);
		String result = new BaseController().welcome(map);
		verify(map).addAttribute(eq("message"), startsWith("Welcome - "));
		verify(map).addAttribute(eq("counter"), anyInt());
	}

	@Test
	public void testWelcomeName() throws Exception {
        	ModelMap map = mock(ModelMap.class);
		String result = new BaseController().welcomeName("test", map);
		verify(map).addAttribute(eq("message"), startsWith("Welcome - test -"));
		verify(map).addAttribute(eq("counter"), anyInt());
	}
	@Test
	public void testAbout() throws Exception {
		String result1 = new About().desc();
		assertTrue("About",result1.contains("This application was copied from somewhere"));
	}

 @Test
        public void testCounterOne() throws Exception {
                int result2 = new Counter().myvar(2,2);
                assertEquals("testCounterOne",1,result2);
        }
 @Test
        public void testCounterTwo() throws Exception {
                int result3 = new Counter().myvar(2,0);
                assertEquals("testCounterTwo",Integer.MAX_VALUE,result3);
        }

}
